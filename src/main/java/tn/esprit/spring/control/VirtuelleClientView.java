package tn.esprit.spring.control;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import tn.esprit.spring.entity.VisiteVirtuelleEntity;
import tn.esprit.spring.service.VisiteVirtuelleServiceImpl;

@Named
@RequestScoped
public class VirtuelleClientView {
		
		 private List<VisiteVirtuelleEntity> visitVirtuelles;  
		 
		 @Autowired 
		 VisiteVirtuelleServiceImpl VisiteVirtuelleService; 
		 
	
	    @PostConstruct
	    public void init() {
	    	visitVirtuelles = VisiteVirtuelleService.retrieveAllVisitesVirtuelles();
	    }

		public List<VisiteVirtuelleEntity> getVisitVirtuelles() {
			return visitVirtuelles;
		}
		
		public void submit(VisiteVirtuelleEntity vr) {
			System.out.println("rat: " + vr.getRating());
			VisiteVirtuelleService.updateVisiteVirtuelle(vr);
				 
		
		}
}

package tn.esprit.spring.control;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FlowEvent;

import tn.esprit.spring.entity.VisiteVirtuelleEntity;
import tn.esprit.spring.service.VisiteVirtuelleService;

@Named
@ViewScoped
public class UpdateVisiteVirtuelle {
	private VisiteVirtuelleEntity visiteVirtuelle;
    
    @Inject
    private VisiteVirtuelleService service;
    
    private Integer id;
    
    private boolean skip;
     
    @PostConstruct
    public void init() {
    	//visitReelles = service.retrieveAllvisitReelles();
    	
    }
    
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
        
        
    }
    
    
 
//    public VisiteVirtuelleEntity getvisiteVirtuelle() {
//        return visitReelle;
//    }
// 
//    public void setService(VisiteReelleService service) {
//        this.service = service;
//    }
    
    public VisiteVirtuelleEntity getVisiteVirtuelle() {
		return visiteVirtuelle;
	}

	public void setService(VisiteVirtuelleService service) {
		this.service = service;
	}

	public void update() {        
    	service.updateVisiteVirtuelle(visiteVirtuelle);
    }
    
    public void loadVisiteVirtuelle(int id) {
    	visiteVirtuelle = service.retrieveVisiteVirtuelle(Long.valueOf(id));
    }
    
    public boolean isSkip() {
        return skip;
    }
 
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
     
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }
    
   
}

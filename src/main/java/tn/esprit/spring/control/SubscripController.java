package tn.esprit.spring.control;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import tn.esprit.spring.entity.Subscription;
import tn.esprit.spring.service.SubscripService;

@Scope(value = "session")
@Controller(value = "subscriptController")
@ELBeanName(value = "subscriptController")
@Join(path= "/l1", to = "/publicab_login.jsf")
public class SubscripController {
	@Autowired
	SubscripService subService;
	private Long id; private String code; private Subscription subscription;
	private Boolean loggedIn;
	public SubscripService getSubService() {
		return subService;
	}
	public void setSubService(SubscripService subService) {
		this.subService = subService;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Subscription getSubscription() {
		return subscription;
	}
	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}
	public Boolean getLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public String doLogin() {
		String navigateTo= "null";
		Subscription subscription=subService.authenticate(id, code);
		if(subscription!= null) {
		navigateTo= "/pages/user/amir.xhtml?faces-redirect=true";
		loggedIn= true; }
		else{
		FacesMessage facesMessage=
		new FacesMessage("Login Failed: please check your id/code and try again.");
		FacesContext.getCurrentInstance().addMessage("form:btn",facesMessage);
		}
		return navigateTo;
		}
		public String doLogout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return"/publicab_login.xhtml?faces-redirect=true";
		}
}

package tn.esprit.spring.control;

public class Bean {
	
	private Double rating;
	 
    public Double getRating() {
        return rating;
    }
    public void setRating(Double rating) {
        this.rating = rating;
    }

}

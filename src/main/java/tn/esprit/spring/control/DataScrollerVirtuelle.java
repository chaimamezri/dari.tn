package tn.esprit.spring.control;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import tn.esprit.spring.entity.VisiteVirtuelleEntity;
import tn.esprit.spring.service.CarService;
import tn.esprit.spring.service.EmailVisiteService;
import tn.esprit.spring.service.VisiteReelleService;
import tn.esprit.spring.service.VisiteVirtuelleService;

@Named
@ViewScoped
public class DataScrollerVirtuelle implements Serializable {
	 	


private List<VisiteVirtuelleEntity> VisitesVirtuelles;

@Inject
   private VisiteVirtuelleService service;


public List<VisiteVirtuelleEntity> getVisitesVirtuelles() {
    return service.retrieveAllVisitesVirtuelles();
}

public void setService(VisiteVirtuelleService service) {
    this.service = service;
}

public void delete(Long id) {        
	service.deleteVisiteVirtuelle(id);
	
}
}
	
package tn.esprit.spring.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.esprit.spring.entity.Subscription;
@Repository
public interface SubscriptionRepository extends CrudRepository<Subscription,Long> {
	
	@Query(nativeQuery=true,value= "update Subscription s set s.prix = (:Datefin-:Datedebut)*1200")
	public void update(@Param("Datefin") Date Datefin,@Param("Datedebut") Date Datedebut);
	@Query("SELECT f FROM Subscription f WHERE f.id=:id and f.code=:code")
	public Subscription getSubByIdAndCode(@Param("id")Long id, @Param("code")String code);


}

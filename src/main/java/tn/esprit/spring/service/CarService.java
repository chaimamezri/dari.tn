package tn.esprit.spring.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import tn.esprit.spring.entity.VisitReelleEntity;
import tn.esprit.spring.repository.VisiteReelleRepo;

@Named
@ApplicationScoped
public class CarService {
	     
		@Autowired
		VisiteReelleRepo visiteReelleRepo;
		
	    public List<VisitReelleEntity> getForUser() {
	    	
	    	Iterator<VisitReelleEntity> i = visiteReelleRepo.findAll().iterator();
	    	
	    	List<VisitReelleEntity> result = new ArrayList<VisitReelleEntity>();
	    	
	    	while(i.hasNext()) {
	    		VisitReelleEntity vr = i.next();
	    		
	    		result.add(vr);
	    	}
	    	
	    	return result;
	    }
}

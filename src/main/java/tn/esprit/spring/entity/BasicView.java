package tn.esprit.spring.entity;

import javax.faces.bean.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class BasicView {
	private String text;
	 
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

}
